'''
Created on Nov 7, 2017

@author: diana
'''


class Book(object):
    def __init__(self, book_id, title, description, author):
        self.__book_id = book_id
        self.__title = title
        self.__description = description
        self.__author = author
    #output: the id of the Book
    #this function will return the id of the book
    def get_book_title(self):
        return self.__title
    def get_book_description(self):
        return self.__description
    def get_book_author(self):
        return self.__author
    def get_book_id(self):
        return self.__book_id
    #this function will update/change the book that has a certain id 
    def update(self, new_title, new_description, new_author):
        self.__title = new_title
        self.__description = new_description
        self.__author = new_author
    #it will return all the caracteristic of a certain book
    def __str__(self):
        return "Book with ID " + str(self.__book_id) + ", title " + str(self.__title) + ", description " + str(self.__description) + " and author " + str(self.__author)