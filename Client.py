'''
Created on Nov 7, 2017

@author: diana
'''


class Client(object):
    def __init__(self, client_id, name):
        self.__client_id = client_id
        self.__client_name = name
    #this function will return the id of the client
    def get_client_name(self):
        return self.__client_name
    def get_client_id(self):
        return self.__client_id
#this function will update/change the client that has a certain id 
    def update(self, new_client_name):
        self.__client_name = new_client_name

    def __str__(self):
        return "Client with ID " + str(self.__client_id) + " and name " + str(self.__client_name)