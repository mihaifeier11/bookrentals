'''
Created on Nov 7, 2017

@author: diana
'''

from Client import Client
from Book import Book
from Rental import Rental


class Repository_Client(object):
    def __init__(self):
#         self.__books = []
        self.__clients = []
#         self.__rentals = []

    def exists_client(self, client_id):
        for client in self.__clients:
            if client.get_client_id() == client_id:
                return True
        return False

    def add_client(self, new_client_id, new_client_name):
        if self.exists_client(new_client_id):
            return False
        self.__clients.append(Client(new_client_id, new_client_name))
        return True

    def remove_client(self, old_client_id):
        for i in range(0, len(self.__clients)):
            if self.__clients[i].get_client_id() == old_client_id():
                self.__clients.pop(i)
                return True
        return False

    def update_client(self, client_id, new_client_name):
        for i in range(0, len(self.__clients)):
            if self.__clients[i].get_client_id() == client_id:
                self.__clients[i].update(new_client_name)
                return True
        return False
    def get_clients(self):
        return self.__clients
    def find_clients(self, arg):
        finds =[]
        for i in range(0,len(self.__clients)):
#         for client in self.__clients:

            if self.__clients[i].get_client_id().find(arg) != -1:
                finds.append(self.__clients[i])
            if self.__clients[i].get_client_name().find(arg) != -1:
                finds.append(self.__clients[i])
        return finds
                

#     def exists_book(self, book_id):
#         for book in self.__books:
#             if book.get_book_id() == book_id:
#                 return True
#         return False
#  
#     def add_book(self, new_book_id, new_book_title, new_book_description, new_book_author):
#         if self.exists_book(new_book_id):
#             return False
#         self.__books.append(Book(new_book_id, new_book_title, new_book_description, new_book_author))
#         return True
#  
#     def remove_book(self, old_book_id):
#         for i in range(0, len(self.__books)):
#             if self.__books[i].get_book_id() == old_book_id:
#                 self.__books.pop(i)
#                 return True
#         return False
#  
#      def update_book(self, book_id, new_book_title, new_book_description, new_book_author):
#          for i in range(0, len(self.__books)):
#              if self.__books[i].get_book_id() == book_id:
#                  self.__books[i].update(new_book_title, new_book_description, new_book_author)
#                  return True
#          return False
#  
#      def get_rentals_by_book_id(self, book_id):
#          result_list = []
#          for rental in self.__rentals:
#              if rental.get_book_id() == book_id:
#                  result_list.append(Rental(rental.get_rental_id(), rental.get_book_id(), rental.get_client_id(), rental.get_rented_date(), rental.get_due_date, rental.get_returned_date()))
#          return result_list
#  
#     def get_rentals_by_client_id(self, client_id):
#         result_list = []
#         for rental in self.__rentals:
#             if rental.get_client_id() == client_id:
#                 result_list.append(Rental(rental.get_rental_id(), rental.get_book_id(), rental.get_client_id(), rental.get_rented_date(), rental.get_due_date, rental.get_returned_date()))
#         return result_list
#  
#      def exists_rental(self, rental_id):
#         for rental in self.__rentals:
#             if rental.get_rental_id() == rental_id:
#                 return True
#         return False
#  
#     def book_already_rented(self, book_id):
#         for rental in self.__rentals:
#             if rental.get_book_id() == book_id and rental.get_return_date() is None:
#                 return True
#         return False
#  
#      def add_rental(self, new_rental_id, new_rental_book_id, new_rental_client_id, new_rental_rented_date, new_rental_due_date):
#         if self.exists_rental(new_rental_id):
#             return False
#         if not self.exists_book(new_rental_book_id):
#             return False
#         if not self.exists_client(new_rental_client_id):
#             return False
#         if self.book_already_rented(new_rental_book_id):
#             return False
#         self.__rentals.append(Rental(new_rental_id, new_rental_book_id, new_rental_client_id, new_rental_rented_date, new_rental_due_date))
#         return True
#  
#     def rental_return_book(self, client_id, book_id, return_date):
#         for rental in self.__rentals:
#             if rental.get_book_id() == book_id and rental.get_client_id() == client_id:
#                 rental.set_return_date(return_date)
#                 return True
#         return False
# 
#     def get_books(self):
#         return self.__books
# 
#     
# 
#     def get_rentals(self):
#         return self.__rentals
