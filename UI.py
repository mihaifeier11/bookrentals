'''
Created on Nov 7, 2017

@author: diana
'''
'''
Created on Nov 7, 2017

@author: diana
'''

from Controller import Controller
from Controller_Book import Controller_Book
from Controller_Rental import Controller_Rental

class UI(object):
    def __init__(self, controller, cont_book, cont_rental):
        self.__controller = controller
        self.__cont_book = cont_book
        self.__cont_rental = cont_rental

    def _print_main_menu(self):
        print("\nThis is the main menu")
        print("\t1. Manage books")
        print("\t2. Manage clients")
        print("\t3. Manage rentals")
        print("\t0. Exit")

    def _print_books_menu(self):
        print("\nThis is the books menu")
        print("\t1. Add a book to the repository")
        print("\t2. Remove a book from the repository")
        print("\t3. Update a book in the repository")
        print("\t4. List all the books stored")
        print("\t5. Find a book")
        print("\t0. Back to the main menu")

    def _print_clients_menu(self):
        print("\nThis is the clients menu")
        print("\t1. Add a client to the repository")
        print("\t2. Remove a client from the repository")
        print("\t3. Update a client in the repository")
        print("\t4. List all the clients stored")
        print("\t5. Find a client")
        print("\t0. Back to the main menu")

    def _print_rentals_menu(self):
        print("\nThis is the rentals main menu")
        print("\t1. Rent a book")
        print("\t2. Return a book")
        print("\t3. List all the rentals stored")
        print("\t0. Back to the main menu")

    def _get_option(self):
        return input("Choose an option: ")

    def main_menu(self):
        #print("Laboratory 5 --- Diana Mocanu --- Group 914\n")
        print("Hello :)\n")
        option = -1
        while option != 0:
            self._print_main_menu()
            option = self._get_option()

            if option == '1':
                self._books_menu()

            elif option == '2':
                self._clients_menu()

            elif option == '3':
                self._rentals_menu()

            elif option == '0':
                print("Bye bye! :)\n")
                return

            else:
                print("Invalid option! Choose again:\n")

    def _books_menu(self):
        option = -1
        while option != '0':
            self._print_books_menu()
            option = self._get_option()

            if option == '1':
                self._add_book()

            elif option == '2':
                self._remove_book()

            elif option == '3':
                self._update_book()

            elif option == '4':
                self._list_books()
            elif option == '5':
                self.ui_find_book()

            elif option == '0':
                return

            else:
                print("Invalid option! Choose again:\n")

    def _clients_menu(self):
        option = -1
        while option != '0':
            self._print_clients_menu()
            option = self._get_option()

            if option == '1':
                self._add_client()

            elif option == '2':
                self._remove_client()

            elif option == '3':
                self._update_client()

            elif option == '4':
                self._list_clients()
            elif option == '5':
                self.ui_find_client()

            elif option == '0':
                return

            else:
                print("Invalid option! Choose again:\n")

    def _rentals_menu(self):
        option = -1
        while option != '0':
            self._print_rentals_menu()
            option = self._get_option()
            
            if option == '1':
                self._rent()

            elif option == '2':
                self._return()

            elif option == '3':
                self._list_rentals()

            elif option == '0':
                return

            else:
                print("Invalid option! Choose again:\n")

    def _add_book(self):
        book_id = input("Give book id: ")
        book_title = input("Give book title: ")
        book_description = input("Give book description: ")
        book_author = input("Give book author: ")
        
        if self.__cont_book.add_book(book_id, book_title, book_description, book_author):
            print("Book added successfully! :)\n")
        else:
            print("Book already stored! :(\n")
        return

    def _remove_book(self):
        book_id = input("Give book id: ")
        if self.__cont_book.remove_book(book_id):
            print("Book successfully removed! :)\n")
        else:
            print("The book does not exist! :(\n")
        return

    def _update_book(self):
        book_id = input("Give book id: ")
        new_book_title = input("Give new book title: ")
        new_book_description = input("Give new book description: ")
        new_book_author = input("Give new book author: ")
        if self.__cont_book.update_book(book_id, new_book_title, new_book_description, new_book_author):
            print("Book successfully updated! :)\n")
        else:
            print("The book does not exist! :(\n")
        return
    def ui_find_book(self):
        arg = input(" please enter by what we want to search the client:")
        finds = self.__cont_book.find_book(arg)
        for find in finds:
                print(str(find))
    def _add_client(self):
        client_id = input("Give client id: ")
        client_name = input("Give client name: ")
        if self.__controller.add_client(client_id, client_name):
            print("Client successfully added! :)\n")
        else:
            print("The client is already stored! :(\n")
        return

    def _remove_client(self):
        client_id = input("Give client id: ")
        if self.__controller.remove_client(client_id):
            print("Client successfully removed! :)\n")
        else:
            print("The client does not exist! :(\n")
        return

    def _update_client(self):
        client_id = input("Give client id: ")
        new_client_name = input("Give new client name: ")
        if self.__controller.update_client(client_id, new_client_name):
            print("Client successfully updated! :)\n")
        else:
            print("The client does not exist! :(\n")
        return
    def ui_find_client(self):
        arg = input(" please enter by what we want to search the client:")
        finds = self.__controller.find_client(arg)
        for find in finds:
                print(str(find))
    def _rent(self):
        rental_id = input("Give rental id: ")
        book_id = input("Give book id: ")
        client_id = input("Give client id: ")
        rented_date = input("Give the rent date: ")
        due_date = input("Give the due date for returning: ")
        if self.__cont_rental.add_rental(rental_id, book_id, client_id, rented_date, due_date):
            print("Book successfully rented! :)\n")
        else:
            print("The rental could not be made! :(\n")
        return

    def _return(self):
        client_id = input("Give client id: ")
        book_id = input("Give book id: ")
        return_date = input("Give the return date: ")
        if self.__cont_rental.rental_return_book(client_id, book_id, return_date):
            print("Book successfully returned! :)\n")
        else:
            print("The book was not rented! :(\n")
        return

    def _list_books(self):
        book_list = self.__cont_book.get_books()
        if len(book_list) == 0:
            print("There is no book stored! :(\n")
        else:
            for book in book_list:
                print(str(book))

    def _list_clients(self):
        client_list = self.__controller.get_clients()
        if len(client_list) == 0:
            print("There is no client stored! :(\n")
        else:
            for client in client_list:
                print(str(client))

    def _list_rentals(self):
        rental_list = self.__cont_rental.get_rentals()
        if len(rental_list) == 0:
            print("There is no rental stored! :(\n")
        else:
            for rental in rental_list:
                print(str(rental))
                