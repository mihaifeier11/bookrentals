'''
Created on Nov 7, 2017

@author: diana
'''
from Controller_Book import Controller_Book
from Controller_Rental import Controller_Rental
'''
Created on Nov 7, 2017

@author: diana
'''

from UI import UI
from Controller import Controller
from Repository_Book import Repository_Book
from Repository_Client import Repository_Client
from Repository_Rental import Repository_Rental
from Book import Book
from Client import Client
from Rental import Rental




def main():
   
    repository = Repository_Book()
    repository.add_book("1", "Poezii", "Volum de poezii", "Mihai Eminescu")
    repository.add_book("2", "Amintiri din copilarie", "Povestiri din copilaria autorului", "Ion Creanga")
    repository.add_book("3", "Divergent", "First book of the trilogy", "Veronica Roth")
    repository.add_book("4", "Insurgent", "Second book of the trilogy", "Veronica Roth")
    repository.add_book("5", "Experiment", "Third book of the trilogy", "Veronica Roth")
    repos_client = Repository_Client()
    repos_client.add_client("1", "John Doe")
    repos_client.add_client("2", "Ion Pop")
    repos_client.add_client("3", "Vasile Baciu")
    repos_client.add_client("4", "Stefan Gheorghidiu")
    repos_client.add_client("5", "Mara Gheorghescu")
    repos_rental = Repository_Rental()

    controller = Controller(repos_client)
    cont_book = Controller_Book(repository)
    cont_rental = Controller_Rental(repos_rental)
    
    ui = UI(controller, cont_book, cont_rental)
#     ui_b = UI(controller, cont_book, cont_rental)
#  '   ui_r = UI(cont_rental)

    ui.main_menu()
#     ui_b.main_menu()
#     ui_r.main_menu()

if __name__ == "__main__":
    main()
