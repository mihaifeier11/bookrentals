'''
Created on Nov 17, 2017

@author: diana
'''
class Controller_Book(object):
    def __init__(self, repository):
        self.__repository = repository
    def add_book(self, new_book_id, new_book_title, new_book_description, new_book_author):
        return self.__repository.add_book(new_book_id, new_book_title, new_book_description, new_book_author)

    def remove_book(self, old_book_id):
        return self.__repository.remove_book(old_book_id)

    def update_book(self, book_id, new_book_title, new_book_description, new_book_author):
        return self.__repository.update_book(book_id, new_book_title, new_book_description, new_book_author)
    def get_books(self):
        return self.__repository.get_books()
    def find_book(self,arg):
        find =[]
        find = self.__repository.find_books(arg)
        if len(find) == 0:
            return" there isn't such a book to be found"
        return find