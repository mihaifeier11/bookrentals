'''
Created on Nov 17, 2017

@author: diana
'''
from Book import Book
class Repository_Book(object):
    def __init__(self):
        self.__books = []
    def exists_book(self, book_id):
        for book in self.__books:
            if book.get_book_id() == book_id:
                return True
            
        return False
    def add_book(self, new_book_id, new_book_title, new_book_description, new_book_author):
        if self.exists_book(new_book_id):
            return False
        self.__books.append(Book(new_book_id, new_book_title, new_book_description, new_book_author))
        return True
    def remove_book(self, old_book_id):
        for i in range(0, len(self.__books)):
            if self.__books[i].get_book_id() == old_book_id:
                self.__books.pop(i)
                return True
        return False
    def update_book(self, book_id, new_book_title, new_book_description, new_book_author):
        for i in range(0, len(self.__books)):
            if self.__books[i].get_book_id() == book_id:
                self.__books[i].update(new_book_title, new_book_description, new_book_author)
                return True
        return False
    def get_books(self):
        return self.__books
        
    def find_books(self, arg):
        finds = []
        for i in range(0, len(self.__books)):
            if self.__books[i].get_book_id().find(arg) != -1:
                finds.append(self.__books[i])
            if self.__books[i].get_book_title().find(arg) != -1:
                finds.append(self.__books[i])
            if self.__books[i].get_book_author().find(arg) != -1:
                finds.append(self.__books[i])
            if self.__books[i].get_book_description().find(arg) != -1:
                finds.append(self.__books[i])
        return finds
