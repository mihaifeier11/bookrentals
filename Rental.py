'''
Created on Nov 7, 2017

@author: diana
'''

class Rental(object):
    def __init__(self, rental_id, book_id, client_id, rented_date, due_date, returned_date=None):
        self.__rental_id = rental_id
        self.__book_id = book_id
        self.__client_id = client_id
        self.__rented_date = rented_date
        self.__due_date = due_date
        self.__returned_date = returned_date
#this function will return  the rental_id
    def get_rental_id(self):
        return self.__rental_id

    def get_book_id(self):
        return self.__book_id

    def get_client_id(self):
        return self.__client_id

    def get_return_date(self):
        return self.__returned_date

    def set_return_date(self, return_date):
        self.__returned_date = return_date

    def __str__(self):
        string = "Rental with ID : " + str(self.__rental_id) + ", book ID :" + str(self.__book_id) + ", client ID :" + str(self.__client_id) + ", rented on :" + str(self.__rented_date) + ", with due date : " + str(self.__due_date)
        if self.__returned_date is None:
            string += " was not returned yet"
        else:
            string += " was returned on "
            string += str(self.__returned_date)
        return string