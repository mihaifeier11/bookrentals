
'''
Created on Nov 7, 2017

@author: diana
'''


from Repository_Book import Repository_Book
from Repository_Client import Repository_Client
from Repository_Rental import Repository_Rental


class Controller(object):
    def __init__(self, repository):
        self.__repository = repository

    def add_client(self, new_client_id, new_client_name):
        return self.__repository.add_client(new_client_id, new_client_name)

    def remove_client(self, old_client_id):
        return self.__repository.remove_client(old_client_id)

    def update_client(self, client_id, new_client_name):
        return self.__repository.update_client(client_id, new_client_name)
    def get_clients(self):
        return self.__repository.get_clients()
    def find_client(self,arg):
        find =[]
        find = self.__repository.find_clients(arg)
        if len(find) == 0:
            return" there isn't such a client to be found"
        return find

#     def add_book(self, new_book_id, new_book_title, new_book_description, new_book_author):
#         return self.__repository.add_book(new_book_id, new_book_title, new_book_description, new_book_author)
# 
#     def remove_book(self, old_book_id):
#         return self.__repository.remove_book(old_book_id)
# 
#     def update_book(self, book_id, new_book_title, new_book_description, new_book_author):
#         return self.__repository.update_book(book_id, new_book_title, new_book_description, new_book_author)
# 
#     def add_rental(self, new_rental_id, new_rental_book_id, new_rental_client_id, new_rental_rented_date, new_rental_due_date):
#         return self.__repository.add_rental(new_rental_id, new_rental_book_id, new_rental_client_id, new_rental_rented_date, new_rental_due_date)
# 
#     def rental_return_book(self, client_id, book_id, return_date):
#         return self.__repository.rental_return_book(client_id, book_id, return_date)
# 
#     def get_books(self):
#         return self.__repository.get_books()
# 
#     def get_clients(self):
#         return self.__repository.get_clients()
# 
#     def get_rentals(self):
#         return self.__repository.get_rentals()
