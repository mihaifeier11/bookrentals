'''
Created on Nov 17, 2017

@author: diana
'''
class Controller_Rental:
    def __init__(self, repository):
        self.__repository = repository
    
    def add_rental(self, new_rental_id, new_rental_book_id, new_rental_client_id, new_rental_rented_date, new_rental_due_date):
        return self.__repository.add_rental(new_rental_id, new_rental_book_id, new_rental_client_id, new_rental_rented_date, new_rental_due_date)

    def rental_return_book(self, client_id, book_id, return_date):
        return self.__repository.rental_return_book(client_id, book_id, return_date)

#     def get_books(self):
#         return self.__repository.get_books()
# 
#     def get_clients(self):
#         return self.__repository.get_clients()

    def get_rentals(self):
        return self.__repository.get_rentals()
