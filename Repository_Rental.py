'''
Created on Nov 17, 2017

@author: diana
'''
from Rental import Rental
from Book import Book
from Client import Client
class Repository_Rental(object):
    def __init__(self):
        self.__rentals = []
    def get_rentals_by_book_id(self, book_id):
        result_list = []
        for rental in self.__rentals:
            if rental.get_book_id() == book_id:
                result_list.append(Rental(rental.get_rental_id(), rental.get_book_id(), rental.get_client_id(), rental.get_rented_date(), rental.get_due_date, rental.get_returned_date()))
        return result_list

    def get_rentals_by_client_id(self, client_id):
        result_list = []
        for rental in self.__rentals:
            if rental.get_client_id() == client_id:
                result_list.append(Rental(rental.get_rental_id(), rental.get_book_id(), rental.get_client_id(), rental.get_rented_date(), rental.get_due_date, rental.get_returned_date()))
        return result_list

    def exists_rental(self, rental_id):
        for rental in self.__rentals:
            if rental.get_rental_id() == rental_id:
                return True
        return False

    def book_already_rented(self, book_id):
        for rental in self.__rentals:
            if rental.get_book_id() == book_id and rental.get_return_date() is None:
                return True
        return False

    def add_rental(self, new_rental_id, new_rental_book_id, new_rental_client_id, new_rental_rented_date, new_rental_due_date):
        if self.exists_rental(new_rental_id):
            return False
        if not self.exists_book(new_rental_book_id):
            return False
        if not self.exists_client(new_rental_client_id):
            return False
        if self.book_already_rented(new_rental_book_id):
            return False
        self.__rentals.append(Rental(new_rental_id, new_rental_book_id, new_rental_client_id, new_rental_rented_date, new_rental_due_date))
        return True

    def rental_return_book(self, client_id, book_id, return_date):
        for rental in self.__rentals:
            if rental.get_book_id() == book_id and rental.get_client_id() == client_id:
                rental.set_return_date(return_date)
                return True
        return False
    def get_rentals(self):
        return self.__rentals
